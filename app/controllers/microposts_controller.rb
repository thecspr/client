class MicropostsController < ApplicationController
  def destroy
    @micropost.destroy
    redirect_to root_url
  end
end