class SessionsController < ApplicationController
	def new
  end

  def create
    req_params = "client_id=#{ENV['oauth_token']}&client_secret=#{ENV['oauth_secret']}&code=#{params[:code]}&grant_type=authorization_code&redirect_uri=#{ENV['oauth_redirect_uri']}"
    response = JSON.parse RestClient.post("#{ENV['server_base_url']}/oauth/token", req_params)
    session[:access_token] = response['access_token']
    user=response['user']
    @user=User.new(name:user['name'], email:user['email'])
    microposts = response['microposts']
    microposts.each do |micropost|
      @micropost = @user.microposts.build(micropost)
    end
    @user.save
    sign_in(@user)
    redirect_to root_path
  end

  def destroy
    session.delete(:access_token )
    sign_out
    redirect_to root_url
  end

end
