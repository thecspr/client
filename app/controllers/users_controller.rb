class UsersController < ApplicationController
  before_action :signed_in_user, only: [:index, :edit, :update, :destroy]

  before_action :restrict_registration, only: [:new, :create]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def edit
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted."
    redirect_to users_url
  end


  
  def new
    
    @user = User.new

  end
  
  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation,:admin)
    end

end
