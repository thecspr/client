require 'spec_helper'

describe "Static pages" do

  subject { page }


  describe "Home page" do

    it "should have the content 'Sample App'" do
      visit '/static_pages/home'
      expect(page).to have_content('Welcome to')
    end

  end

end
